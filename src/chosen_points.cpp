#include <chosen_points.h>
#include <algorithm>
#include <ctime>

ChosenPoints::ChosenPoints(sf::Vector2u window_size)
    : NormalizedPoints(window_size, 0, 3)
{
    std::srand(unsigned(std::time(0)));
}

void ChosenPoints::insertAndNormalize(sf::Vector2f point, bool type)
{
    NormalizedPoints::insertAndNormalize(point);
    ai_library::Vector &new_point = operator [](size() - 1);
    type==0 ? new_point(2) = 0.0f : new_point(2) = 1.0f;
}
