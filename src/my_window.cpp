#include <my_window.h>
#include <points_data.h>
#include <neural_net.h>
#include <cassert>

MyWindow::MyWindow(const char *name)
    : sf::RenderWindow(sf::VideoMode::getDesktopMode(), name), isEventWaiting(false), neuralNetFill(false)
{

}

bool MyWindow::grabNextEvent(sf::Event &event)
{
    if(!isOpen())
        return false;
    if(isEventWaiting)
    {
        isEventWaiting = false;
        return true;
    }
    assert(isEventWaiting == false);
    return waitEvent(event);
}

bool MyWindow::handleEventKeyPressed(sf::Event &event)
{
    if(event.key.code == sf::Keyboard::Key::Space)
    {
        neuralNetFill = true;
        return true;
    }
    if(event.key.code == sf::Keyboard::Key::Escape)
        close();
    return false;
}

void MyWindow::handleEventClosed(sf::Event &event)
{
    (void)event;
    close();
}

void MyWindow::handleEventResized(sf::Event &event, PointsData &points_data)
{
    neuralNetFill = false;
    unsigned width = event.size.width;
    unsigned height = event.size.height;
    while(pollEvent(event)) // finish if no event occurs
    {
        if(event.type != sf::Event::Resized)
        {
            points_data.setNewWindowSize(sf::Vector2u(width, height));
            setView(sf::View(sf::FloatRect(0, 0, width, height)));
            isEventWaiting = true;
            return;
        }
        width = event.size.width;
        height = event.size.height;
    }
    points_data.setNewWindowSize(sf::Vector2u(width, height));
    setView(sf::View(sf::FloatRect(0, 0, width, height)));
}

void MyWindow::handleEventMouseButtonPressed(sf::Event &event, PointsData &points_data)
{
    sf::Vector2f point(static_cast<float>(event.mouseButton.x), static_cast<float>(event.mouseButton.y));
    if(event.mouseButton.button == sf::Mouse::Button::Left)
    {
        points_data.chosenPoints.insertAndNormalize(point, 0);
        neuralNetFill = false;
    }
    else if(event.mouseButton.button == sf::Mouse::Button::Right)
    {
        points_data.chosenPoints.insertAndNormalize(point, 1);
        neuralNetFill = false;
    }
}

void MyWindow::drawAll(const PointsData &points_data)
{
    if(neuralNetFill)
        drawPointsClassifiedByNeuralNetwork(points_data);
    else
        drawDefaultFill();
    drawChosenPoints(points_data);
}

void MyWindow::drawDefaultFill()
{
    clear(sf::Color(230, 230, 230));
}

void MyWindow::drawChosenPoints(const PointsData &points_data)
{
    for(const auto &p : points_data.chosenPoints)
    {
        sf::Vector2f point;
        points_data.chosenPoints.denormalize(p, point);
        sf::CircleShape shape(POINT_RADIUS);
        shape.setOrigin(POINT_RADIUS, POINT_RADIUS);
        if(p(2) == 0.0f)
            shape.setFillColor(sf::Color(0, 0, 0));
        else
            shape.setFillColor(sf::Color(0, 0, 255));
        shape.setPosition(point.x, point.y);
        draw(shape);
    }
}

void MyWindow::drawPointsClassifiedByNeuralNetwork(const PointsData &points_data)
{
    draw(points_data.pointsToRender);
}
