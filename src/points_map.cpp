#include <points_map.h>
#ifndef NDEBUG
#include <iostream>
#endif

PointsMap::PointsMap(sf::Vector2u window_size)
    : NormalizedPoints(window_size)
{
    remap();
}

void PointsMap::remap()
{
//#ifndef NDEBUG
//    std::cerr << "PointsMap::remap: windowSize = (" << windowSize.x << ", " << windowSize.y << ")\n";
//#endif
    clear();
    reserve(windowSize.x * windowSize.y);
    for(unsigned y = 0; y < windowSize.y; ++y)
    {
        for(unsigned x = 0; x < windowSize.x; ++x)
        {
            insertAndNormalize(sf::Vector2f(static_cast<float>(x), static_cast<float>(y)));
        }
    }
}
