#include <my_window.h>
#include <ai_library_helpers.h>
#include <neural_net.h>
#include <neural_net_trainer.h>
#include <points_data.h>
#include <sstream>
#include <vector>

const std::string NEURAL_NET_INIT =
    "0.001\n" // maxInitialWeight
    "0.001\n" // weightsLearningRate
    "0.0001\n" // biasLearningRate
    "1\n" // numberOfHiddenLayers
    "2\n" // inputSize"
    "4 3\n"
    "1 3\n\n"; // the last layer size 1 and activationFunctionNumber 3 (softplus)

int main()
{
    ai_library::NeuralNet nn;
    std::istringstream nn_init(NEURAL_NET_INIT);
    nn.initialize(nn_init);
    ai_library::NeuralNetTrainer trainer(nn);
    MyWindow window("simplenn");
    PointsData points_data(window.getSize());
    sf::Event event;
    while(window.grabNextEvent(event))
    {
        switch(event.type)
        {
            case sf::Event::KeyPressed:
                if(window.handleEventKeyPressed(event))
                    points_data.runNeuralNetPrediction(nn);
                break;

            case sf::Event::Closed:
                window.handleEventClosed(event);
                return 0;

            case sf::Event::Resized:
                window.handleEventResized(event, points_data);
                break;

            case sf::Event::MouseButtonPressed:
                window.handleEventMouseButtonPressed(event, points_data);
                nn.setRandomWeightsAndBias();
                points_data.trainNeuralNet(trainer);
                break;

            default:
                break;
        }
        window.drawAll(points_data);
        window.display();
    }
    return 0;
}
