#include <points_data.h>
#include <neural_net.h>
#include <neural_net_trainer.h>
#include <chrono>

PointsData::PointsData(sf::Vector2u window_size)
    : pointsMap(window_size), chosenPoints(window_size),
      pointsToRender(sf::PrimitiveType::Points, window_size.x * window_size.y)
{
    setUpPointsToRender();
}

void PointsData::setUpPointsToRender()
{
    unsigned n = 0;
    for(unsigned y = 0; y < pointsMap.windowSize.y; ++y)
    {
        for(unsigned x = 0; x < pointsMap.windowSize.x; ++x)
        {
            pointsToRender[n].position.x = static_cast<float>(x);
            pointsToRender[n].position.y = static_cast<float>(y);
            ++n;
        }
    }
}

void PointsData::setNewWindowSize(sf::Vector2u window_size)
{
    pointsMap.windowSize = window_size;
    pointsMap.remap();
    chosenPoints.windowSize = window_size;
    pointsToRender.resize(window_size.x * window_size.y);
    setUpPointsToRender();
}

void PointsData::trainNeuralNet(ai_library::NeuralNetTrainer &neural_net_trainer)
{
    using namespace std::chrono;
    ChosenPointsIterator iterator(chosenPoints);
    struct Functor
    {
        steady_clock::time_point start;
        Functor() : start(steady_clock::now()) {}
        bool operator() () { return duration_cast<milliseconds>(steady_clock::now() - start).count() > 100; }
    } functor;
    neural_net_trainer.train(iterator, functor);
}

void PointsData::runNeuralNetPrediction(ai_library::NeuralNet &neural_net)
{
    assert(pointsMap.size() == pointsToRender.getVertexCount());
    unsigned i = 0;
    for(const auto &p : pointsMap)
    {
        neural_net.propagateForward(p.segment(0,2));
        float output = neural_net.getOutput()(0);
        int tmp = static_cast<int>(510.0f * output);
        sf::Uint8 red = 0, green = 0;
        if(output < 0.5f)
        {
            assert(tmp <= 255);
            red = 255 - tmp;
        }
        else
        {
            assert(tmp >= 255);
            green = tmp - 255;
        }
        pointsToRender[i++].color = sf::Color(red, green, 0);
    }
}

PointsData::ChosenPointsIterator::ChosenPointsIterator(std::vector<ai_library::Vector> &v)
    : vector(v), iterator(vector.begin()), input(2), output(1)
{
    assert(iterator != vector.end());
}

void PointsData::ChosenPointsIterator::operator++()
{
    if(++iterator == vector.end())
    {
        std::random_shuffle(vector.begin(), vector.end());
        iterator = vector.begin();
    }
}

const ai_library::Vector &PointsData::ChosenPointsIterator::getInput()
{
    input << (*iterator)(0), (*iterator)(1);
    return input;
}

const ai_library::Vector &PointsData::ChosenPointsIterator::getOutput()
{
    output << (*iterator)(2);
    return output;
}
