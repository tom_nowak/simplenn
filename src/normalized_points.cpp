#include <normalized_points.h>

namespace
{
    float normalize(float x, float max)
    {
        assert(x < max);
        return x/max;
    }

    unsigned denormalize(float x, float max)
    {
        assert(x >= 0.0f);
        assert(x < 1.0f);
        return x*max;
    }
}

NormalizedPoints::NormalizedPoints(sf::Vector2u window_size, size_t number_of_points, unsigned inserted_point_size)
    : std::vector<ai_library::Vector>(number_of_points, ai_library::Vector(inserted_point_size)),
      windowSize(window_size), pointSize(inserted_point_size)
{
    assert(inserted_point_size >= 2);
}

void NormalizedPoints::normalize(const sf::Vector2f &source, ai_library::Vector &target) const
{
    target(0) = ::normalize(source.x, static_cast<float>(windowSize.x));
    target(1) = ::normalize(source.y, static_cast<float>(windowSize.y));
}

void NormalizedPoints::denormalize(const ai_library::Vector &source, sf::Vector2f &target) const
{
    target.x = ::denormalize(source(0), static_cast<float>(windowSize.x));
    target.y = ::denormalize(source(1), static_cast<float>(windowSize.y));
}

void NormalizedPoints::insertAndNormalize(const sf::Vector2f &point)
{
    emplace_back(ai_library::Vector(pointSize));
    ai_library::Vector &new_point = operator [](size() - 1);
    new_point(0) = ::normalize(point.x, static_cast<float>(windowSize.x));
    new_point(1) = ::normalize(point.y, static_cast<float>(windowSize.y));
}
