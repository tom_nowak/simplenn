### Simple Neural Net demonstration  
Neural Net has been created during project for AI classes on Warsaw University of Technology ([see repository](https://bitbucket.org/tom_nowak/pszt_neural_net)). This program uses the same Neural Net in problem of colouring a plane based on some given points.  

### Requirements  
CMake, C++ compiler (supportiong C++11), libraries: Eigen, SFML.  

### Compilation (Linux):  
```
mkdir build 
cd build
cmake .. 
make  
```

CMake should also work on other systems, creating appropriate project files (for example VisualStudio project on  Windows).   

### Instruction  
Pick two types of points clicking on the image - black points using left mouse button, blue points - right mouse button.  
Press SPACE to let the neural net classify all image points based on the points you chose (the training data) - each point will be given red or green colour.  
Press ESC to quit.
