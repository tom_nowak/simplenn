#pragma once
#include <points_map.h>
#include <chosen_points.h>
#include <SFML/Graphics.hpp>

namespace ai_library
{
    class NeuralNet;
    class NeuralNetTrainer;
}

struct PointsData
{
    PointsMap pointsMap;
    ChosenPoints chosenPoints;
    sf::VertexArray pointsToRender;

    PointsData(sf::Vector2u window_size);
    void setUpPointsToRender();
    void setNewWindowSize(sf::Vector2u window_size);
    void trainNeuralNet(ai_library::NeuralNetTrainer &neural_net_trainer);
    void runNeuralNetPrediction(ai_library::NeuralNet &neural_net);

    class ChosenPointsIterator // for neural net training
    {
    private:
        std::vector<ai_library::Vector> &vector;
        std::vector<ai_library::Vector>::const_iterator iterator;
        ai_library::Vector input;
        ai_library::Vector output;

    public:
        ChosenPointsIterator(std::vector<ai_library::Vector> &v);
        void operator++();
        const ai_library::Vector &getInput();
        const ai_library::Vector &getOutput();
    };
};
