#pragma once
#include <ai_library_helpers.h>
#include <vector>
#include <SFML/System/Vector2.hpp>

// vector of normalized points - each of them has coordinates in range [0,1)
class NormalizedPoints : public std::vector<ai_library::Vector>
{
public:
    sf::Vector2u windowSize;
    const unsigned pointSize; // at least 2, but points may also contain additional data

    NormalizedPoints(sf::Vector2u window_size, size_t number_of_points = 0, unsigned inserted_point_size = 2);
    void normalize(const sf::Vector2f &source, ai_library::Vector &target) const;
    void denormalize(const ai_library::Vector &source, sf::Vector2f &target) const;
    void insertAndNormalize(const sf::Vector2f &point);
};
