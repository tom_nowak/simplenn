#pragma once
#include <normalized_points.h>

// class storing all possible window's coordinates in normalized form
class PointsMap : public NormalizedPoints
{
public:
    PointsMap(sf::Vector2u window_size);
    void remap();
};
