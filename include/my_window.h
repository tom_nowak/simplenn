#pragma once
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

class PointsData; // forward declaration

class MyWindow : public sf::RenderWindow
{
public:
    static constexpr unsigned POINT_RADIUS = 5;

    MyWindow(const char *name);
    bool grabNextEvent(sf::Event &event);
    bool handleEventKeyPressed(sf::Event &event); // return true if space pressed
    void handleEventClosed(sf::Event &event);
    void handleEventResized(sf::Event &event, PointsData &points_data);
    void handleEventMouseButtonPressed(sf::Event &event, PointsData &points_data);
    void drawAll(const PointsData &points_data);

private:
    bool isEventWaiting;
    bool neuralNetFill;

    void drawDefaultFill();
    void drawChosenPoints(const PointsData &points_data);
    void drawPointsClassifiedByNeuralNetwork(const PointsData &points_data);
};
