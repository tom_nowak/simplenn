#pragma once
#include <normalized_points.h>

// class storing points which have been picked by the user
// each point has additional coordinate, which is
// 0 for points of one type
// 1 for points of the other type
class ChosenPoints : public NormalizedPoints
{
public:
    ChosenPoints(sf::Vector2u window_size);
    void insertAndNormalize(sf::Vector2f point, bool type);
};
