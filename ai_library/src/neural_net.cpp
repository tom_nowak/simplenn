#include "neural_net.h"
#include <cmath>
#include <iostream>
#include <chrono>
#include <random>
#include <functional>

using namespace ai_library;

void NeuralNet::initialize(std::istream &input)
{
    IstreamReader reader(input);
    reader.readFromNewLine(maxInitialWeight);
    reader.readFromNewLine(weightsLearningRate);
    reader.readFromNewLine(biasLearningRate);
    reader.readFromNewLine(numberOfHiddenLayers);
    reader.readFromNewLine(inputSize);
    allocateLayers(reader);
    if(reader.getLine())
    {
        readLayersBias(reader);
        readWeights(reader);
    }
    else
        setRandomWeightsAndBias();
}

void NeuralNet::serialize(std::ostream& output) const
{
#define SAVE(x) output << x << " # " << #x << "\n";
    SAVE(maxInitialWeight);
    SAVE(weightsLearningRate);
    SAVE(biasLearningRate);
    SAVE(numberOfHiddenLayers);
    SAVE(inputSize);
#undef SAVE
    for(unsigned i = 0; i < layers.size(); ++i)
    {
        output << layers[i].getSize() << " " << layers[i].activationFunctionNumber <<
                  " # layers[" << i << "] size and activationFunctionNumber\n";
    }
    output << "bias:\n";
    for(const Layer &layer : layers)
        output << layer.bias << "\n";
    for(unsigned i = 0; i < layers.size(); ++i)
        output << "weights[->" << i << "]:\n" << layers[i].weights << "\n";
}

void NeuralNet::propagateForward(const ai_library::Vector &input)
{
    if(inputSize != input.cols())
        throw std::invalid_argument("vector size passed to NeuralNet::propagateForward does not match inputSize in NeuralNet configuration");
    layers[0].process(input);
    for(unsigned i = 1; i < layers.size(); ++i)
        layers[i].process(layers[i-1].output);
}

const ai_library::Vector &NeuralNet::getOutput() const
{
    return layers.back().output;
}

void NeuralNet::setRandomWeightsAndBias()
{
    static std::default_random_engine generator(std::chrono::system_clock::now().time_since_epoch().count());
    std::uniform_real_distribution<float> distribution(-maxInitialWeight, maxInitialWeight);
    for(Layer &layer : layers)
    {
        for(unsigned i = 0; i < layer.getSize(); ++i)
            layer.bias(i) = distribution(generator);
        for(unsigned col = 0; col < layer.weights.cols(); ++col)
        {
            for(unsigned row = 0; row < layer.weights.rows(); ++row)
                layer.weights(row, col) = distribution(generator);
        }
    }
}

void NeuralNet::allocateLayers(IstreamReader &reader)
{
    layers.clear(); // in case of multiple initialize() calls - clear previous data
    unsigned size, activation_function_number;
    unsigned number_of_layers = numberOfHiddenLayers + 1;
    unsigned previous_size = inputSize;
    layers.reserve(number_of_layers);
    for(unsigned i = 0; i < number_of_layers; ++i)
    {
        reader.getLine();
        reader.readFromCurrentLine(size, activation_function_number);
        if(activation_function_number >= activation_functions::size)
            throw std::invalid_argument("unknown activation function number");
        layers.emplace_back(size, previous_size, activation_function_number);
        previous_size = size;
    }
}

void NeuralNet::readLayersBias(IstreamReader &reader)
{
    std::string str;
    reader.readFromCurrentLine(str);
    if(str != "bias:")
        throw std::invalid_argument("incorrect data in configuration file - bias keyword missing");
    for(Layer &layer : layers)
    {
        reader.getLine();
        for(unsigned i = 0; i < layer.getSize(); ++i)
            reader.readFromCurrentLine(layer.bias(i));
    }
}

void NeuralNet::readWeights(IstreamReader &reader)
{
    std::string str;
    unsigned layer_number = 0;
    for(Layer &layer : layers)
    {
        reader.readFromNewLine(str);
        if(str != "weights[->" + std::to_string(layer_number) + "]:")
            throw std::invalid_argument("incorrect data in configuration file - layer number missing");
        for(unsigned row = 0; row < layer.weights.rows(); ++row)
        {
            reader.getLine();
            for(unsigned col = 0; col < layer.weights.cols(); ++col)
                reader.readFromCurrentLine(layer.weights(row, col));
        }
        ++layer_number;
    }
}

NeuralNet::Layer::Layer(unsigned size, unsigned previous_layer_size, unsigned activation_function_number)
    : activationFunctionNumber(activation_function_number), input(size),
      bias(size), output(size), weights(previous_layer_size, size)
{
}

unsigned NeuralNet::Layer::getSize() const
{
    return output.cols();
}

void NeuralNet::Layer::process(const ai_library::Vector &v)
{
#ifndef NDEBUG
    if(v.cols() != weights.rows())
        throw std::invalid_argument("input vector size does not match weights matrix size");
#endif
    input.noalias() = v * weights;
    output.noalias() = (input + bias).unaryExpr(activationFunction());
}

ActivationFunctionType NeuralNet::Layer::activationFunction() const
{
    return activation_functions::functions[activationFunctionNumber];
}

ActivationFunctionType NeuralNet::Layer::activationFunctionDerivative() const
{
    return activation_functions::derivatives[activationFunctionNumber];
}
