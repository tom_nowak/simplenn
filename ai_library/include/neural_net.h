﻿#pragma once
#include <ios>
#include <vector>
#include "ai_library_helpers.h"

namespace ai_library
{
    class IstreamReader;

    class NeuralNet
    {
    public:
        friend class NeuralNetTrainer;

        // Reads NN from input stream. It must contain, in new line each:
        //   maxInitialWeight (floating point)
        //   weightsLearningRate (floating point)
        //   biasLearningRate (floating point)
        //   numberOfHiddenLayers (nonnegative integer)
        //   inputSize (positive integer)
        // Then, (numberOfHiddenLayers + 1) lines with layer data:
        //   layerSize  activationFunctionNumber (see possible functions in ai_library_helpers.h)
        // If network has not yet been created, this should be the end of file.
        // Else, there should be:
        // bias:
        //   row vector with bias in layer i <for each layer number i>
        // Then all weights:
        //   weights[->0]:
        //   matrix - weights coming to 0 layer
        //   ... (the same for each layer)
        void initialize(std::istream &input);

        // Saves NN to a file, producing output that can be read in constructor.
        void serialize(std::ostream& output) const;

        // Input vector must have the same size as first layer.
        // For convenience, function saves values in all neurons, including hidden ones.
        // To obtain output after propagation, call getOutput().
        void propagateForward(const Vector &input);

        // Returns output from the last layer (after last propagation).
        const Vector &getOutput() const;

        // Moved to public to enable neural net "reset"
        void setRandomWeightsAndBias();

    private:
        struct Layer
        {
            const unsigned activationFunctionNumber;
            Vector input;
            Vector bias;
            Vector output; // output = activationFunction(input + bias)
            Matrix weights; // rows - number of neurons in previous layer, cols - number of neurons in this layer

            Layer(unsigned size, unsigned previous_layer_size, unsigned activation_function_number);
            unsigned getSize() const;
            void process(const Vector &v);
            ActivationFunctionType activationFunction() const;
            ActivationFunctionType activationFunctionDerivative() const;
        };

        std::vector<Layer> layers;

        // Parameters used to create neural net (layer sizes are determined by weights, data redundancy is for readability):
        float maxInitialWeight; // number [0, 1] (should be very small); used only in setRandomWeights()
        float weightsLearningRate;
        float biasLearningRate;
        unsigned numberOfHiddenLayers;
        unsigned inputSize; // output size will be determined by the size of the last layer

        // Some private functions:
        // Functions helping create NN:
        void allocateLayers(IstreamReader &reader);
        void readLayersBias(IstreamReader &reader);
        void readWeights(IstreamReader &reader);
    };
}
