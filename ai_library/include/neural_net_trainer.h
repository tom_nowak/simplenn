#pragma once
#include "neural_net.h"

namespace ai_library
{
    class NeuralNetTrainer
    {
    public:
        NeuralNetTrainer(NeuralNet &nn);
        static float getErrorValue(const ai_library::Vector &output, const ai_library::Vector &expected_output);
        using value_type = float;

        // TrainingDataIterator must have methods:
        // operator++() moving iterator one position further
        // const ai_library::Vector &getInput()
        // const ai_library::Vector &getOutput()
        // No begin() or end() are required, this is a circular iterator.
        // StopConditionFunctor must have bool operator(),
        // returning true if training should end.
        template <typename TrainingDataIterator, typename StopConditionFunctor>
        void train(TrainingDataIterator &iterator, StopConditionFunctor &stop_functor)
        {
            do
            {
                // The whole training iteration is performed for a single data entry.
                // By changing all input, output, delta vectors to matrices one could perform treining iteration for multiple
                // data (summing errors from each data entry), but we decided not to do so because our training set is small.
                neuralNet.propagateForward(iterator.getInput());
                setDeltaInOutputLayer(iterator.getOutput());
                propagateBackward();
                updateWeights(iterator.getInput());
                updateLearningRate();
                ++iterator;
            } while(!stop_functor());
        }


    private:
        NeuralNet &neuralNet;
        std::vector<ai_library::Vector> deltas;

        void setDeltaInOutputLayer(const ai_library::Vector &expected_output);
        void propagateBackward();
        void updateWeights(const ai_library::Vector &input);
        void updateLearningRate();
    };
}
